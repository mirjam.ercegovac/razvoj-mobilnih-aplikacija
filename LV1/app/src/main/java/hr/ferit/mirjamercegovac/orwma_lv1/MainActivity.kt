package hr.ferit.mirjamercegovac.orwma_lv1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn = findViewById<Button>(R.id.btn)
        btn.setOnClickListener {
            val etN = findViewById<EditText>(R.id.editTextName)
            val et = findViewById<EditText>(R.id.editText)
            val tvN = findViewById<TextView>(R.id.textViewName)
            val tv = findViewById<TextView>(R.id.textView)

            tvN.setText(etN.text.toString())
            tv.setText(et.text.toString())
        }

        val btnCalc = findViewById<Button>(R.id.btnCalc)
        val etH = findViewById<EditText>(R.id.editTextHeight)
        val etW = findViewById<EditText>(R.id.editTextWeight)
        btnCalc.setOnClickListener {
            if (etH.text.isNotEmpty() && etW.text.isNotEmpty()) {
                val height = (etH.text.toString()).toInt()
                val weight = (etW.text.toString()).toInt()
                val BMI = calculateBMI(height, weight)

                Toast.makeText(this,BMI.toString(), Toast.LENGTH_LONG).show()
            }

            else {
                Toast.makeText(this, "Unesi valjane vrijednosti", Toast.LENGTH_SHORT).show()
            }
        }
    }
    private fun calculateBMI(height: Int, weight: Int): Float {

        val h = height.toFloat() / 100
        val BMI = weight.toFloat() / (h * h)

        return BMI
    }

}

