package hr.ferit.mirjamercegovac.orwma_lv2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class SecondActivity : AppCompatActivity() {
    private val fragmentManager = supportFragmentManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val actionBar = supportActionBar
        actionBar!!.title = "ORWMA_LV2"
        actionBar.setDisplayHomeAsUpEnabled(true)


        findViewById<Button>(R.id.firstButton).setOnClickListener{
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.frameLayout, Fragment1())
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }

        findViewById<Button>(R.id.button2).setOnClickListener {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.frameLayout, Fragment2())
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }

    }
}